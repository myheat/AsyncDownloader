package org.hidaron.example.demo.download.adapter;

import java.util.ArrayList;
import java.util.List;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class DefaultAdapter<T> extends BaseAdapter {
	protected List<T> mList;

	public DefaultAdapter() {
		mList = new ArrayList<T>();
	}

	public DefaultAdapter(List<T> list) {
		this();
		if (null != list) {
			mList = list;
		}
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public T getItem(int position) {
		return mList.get((position < 0 || position > mList.size() - 1) ? 0
				: position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void addItem(T bean) {
		if (null == bean) {
			return;
		}
		mList.add(bean);
	}

	public void addItem(int position, T bean) {
		mList.add(position, bean);
	}

	public void removeItem(T bean) {
		mList.remove(bean);
	}

	public void clear() {
		mList.clear();
	}

	public void addAll(List<T> list) {
		mList.addAll(list);

	}

	public void addAll(int position, List<T> list) {
		mList.addAll(0, list);
	}

	public void setAll(List<T> list) {
		if (null == list) {
			return;
		}
		mList = list;
	}

	public abstract View getView(int position, View convertView,
			ViewGroup parent);
}
