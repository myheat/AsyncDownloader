package org.hidaron.example.demo.download;

import java.util.ArrayList;
import java.util.List;

import org.hidaron.asyncdownloader.R;
import org.hidaron.asyncdownloader.utils.NetWorkUtils;
import org.hidaron.asyncdownloader.utils.ToastUtils;
import org.hidaron.example.demo.download.adapter.DownloadListAdapter;
import org.hidaron.example.demo.download.service.DownloadService;
import org.hidaron.example.demo.download.service.DownloadService.DownloadBinder;

import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

public class AsyncDownloaderFragment extends Fragment {
	private static final String TAG = AsyncDownloaderFragment.class
			.getSimpleName();
	private DownloadService.DownloadBinder downloadBinder;
	private ListView downloadList;
	private List<AppInfo> infoList;

	private ServiceConnection connection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d(TAG, "connect download service success...");
			downloadBinder = (DownloadBinder) service;
			DownloadListAdapter adapter = new DownloadListAdapter(
					getActivity(), infoList, downloadBinder);
			downloadList.setAdapter(adapter);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG, "connect download service shutdown...");
			downloadBinder = null;
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 setHasOptionsMenu(true);
		Intent intent = new Intent(getActivity(), DownloadService.class);
		getActivity().bindService(intent, connection, Context.BIND_AUTO_CREATE);

		infoList = new ArrayList<AppInfo>();
		for (int i = 1; i <= 9; i++) {
			infoList.add(new AppInfo("App" + i, URLResource.URL[i % 4]));
		}
		Log.d(TAG, String.valueOf(downloadBinder));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_asyncdownloader, null);
		downloadList = (ListView) view.findViewById(R.id.download_list);
		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.task_menu, menu);
		Button showTask = (Button) menu.findItem(R.id.show_task_list)
				.getActionView();
		showTask.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getActivity(),
						TaskListActivity.class);
				startActivity(intent);
			}
		});
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public void onStart() {
		super.onStart();
		if (!NetWorkUtils.isCurrentNetWorkConnected(getActivity())) {
			ToastUtils.warning(getActivity(), R.string.network_not_connectable);
		}
	}

	@Override
	public void onDestroy() {
		downloadBinder.cancelAllDownload();
		getActivity().unbindService(connection);
		super.onDestroy();
	}

}
