package org.hidaron.asyncdownloader.utils;

import android.os.Environment;
import android.os.StatFs;

/**
 * 
 * @author hidaron
 * @date 2014-1-8
 * 
 */
public class StorageUtils {

	public static boolean isExternalStorageAvailable() {
		return Environment.getExternalStorageState().equals(
				Environment.MEDIA_REMOVED) ? false : true;
	}

	public static boolean isExternalStoragewritable() {
		return Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED) ? true : false;
	}

	@SuppressWarnings("deprecation")
	public static long getExternalStorageAvailableSize() {
		StatFs sf = new StatFs(Environment.getExternalStorageDirectory()
				.getPath());
		return sf.getBlockSize() * sf.getAvailableBlocks();
	}

	public static String getExternalStorageDownloadsDir() {
		return Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_DOWNLOADS).getPath();
	}

}
