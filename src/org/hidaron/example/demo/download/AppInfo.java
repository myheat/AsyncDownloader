package org.hidaron.example.demo.download;

public class AppInfo {

	private String appUrl;
	private String appName;

	public AppInfo(String appName, String appUrl) {
		this.appUrl = appUrl;
		this.appName = appName;
	}

	public String getAppUrl() {
		return appUrl;
	}

	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

}
