package org.hidaron.asyncdownloader.download.listener;

/**
 * 
 * @author hidaron
 * 
 */
public interface OnDownloadListener {

	public void onStart(int contentLength, int writtenBytesLength,
			long firstTime);

	public void onSuccess(int statusCode, byte[] responseBody);

	public void onFailure(int statusCode, byte[] responseBody, Throwable error);

	public void onProgress(int wirttenBytesLength, int totalLength);

	public void onPaused(int writtenBytesLength);

	public void onFinished(String tragetFilePatch);

	public void onCanceled();

}
