package org.hidaron.asyncdownloader.download;

import org.apache.http.Header;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.ResponseHandlerInterface;

/**
 * 
 * @author hidaron <hidaron@163.com>
 * 
 */
public class DownloadAsyncHttpClient extends AsyncHttpClient {

	public RequestHandle get(String url, Header[] headers,
			ResponseHandlerInterface responseHandler) {

		HttpUriRequest request = new HttpGet(url);
		request.setHeaders(headers);
		return sendRequest((DefaultHttpClient) getHttpClient(),
				getHttpContext(), request, null, responseHandler, null);
	}
}
