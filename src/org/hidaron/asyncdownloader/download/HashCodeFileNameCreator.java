package org.hidaron.asyncdownloader.download;

/**
 * 
 * @author hidaron <hidaron@163.com>
 *
 */
public class HashCodeFileNameCreator implements FileNameCreator {

	@Override
	public String greateName(String url) {
		return String.valueOf(url.hashCode());
	}

}
