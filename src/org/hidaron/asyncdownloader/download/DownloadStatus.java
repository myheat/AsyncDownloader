package org.hidaron.asyncdownloader.download;

public enum DownloadStatus {

	NOTSTART, START, DOWNLOADING, PAUSED, CANCELED, FINISHED, FAILURE, WAITING;
}
