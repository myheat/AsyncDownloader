package org.hidaron.asyncdownloader.utils;

import org.hidaron.asyncdownloader.R;

import android.app.Activity;
import android.view.Gravity;

import com.devspark.appmsg.AppMsg;

/**
 * 
 * @author hidaron
 * @date 2014-1-7
 * 
 */
public class ToastUtils {

	public static void warning(Activity context, CharSequence text) {
		AppMsg appMsg = AppMsg.makeText(context, text, AppMsg.STYLE_ALERT);
		appMsg.setLayoutGravity(Gravity.BOTTOM);
		appMsg.show();
	}

	public static void warning(Activity context, int resId) {
		AppMsg appMsg = AppMsg.makeText(context, resId, AppMsg.STYLE_ALERT);
		appMsg.setLayoutGravity(Gravity.BOTTOM);
		appMsg.show();
	}

	public static void info(Activity context, CharSequence text) {
		AppMsg appMsg = AppMsg.makeText(context, text, new AppMsg.Style(
				AppMsg.LENGTH_SHORT, R.color.holo_purple));
		appMsg.setLayoutGravity(Gravity.BOTTOM);
		appMsg.show();
	}

	public static void info(Activity context, int resId) {
		AppMsg appMsg = AppMsg.makeText(context, resId, new AppMsg.Style(
				AppMsg.LENGTH_SHORT, R.color.holo_purple));
		appMsg.setLayoutGravity(Gravity.BOTTOM);
		appMsg.show();
	}

}
