package org.hidaron.asyncdownloader.download;

import java.io.File;

import android.os.Environment;

public class DownloaderOptions {
	public static final File DEFAULT_DOWNLOADS_DIR = Environment
			.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
	public static final File CACHE_DOWNLOADS_DIR = Environment
			.getDownloadCacheDirectory();

	private final File mDownloadsDir;
	private final FileNameCreator mFileNameScheme;

	private DownloaderOptions(Builder Builder) {
		mDownloadsDir = Builder.DownloadsDir;
		mFileNameScheme = Builder.fileNameCreator;
	}

	public File getDownloadsDir() {
		return mDownloadsDir;
	}

	public FileNameCreator getFileNameGreator() {
		return mFileNameScheme;
	}

	public static class Builder {
		private File DownloadsDir;
		private FileNameCreator fileNameCreator;

		public Builder() {
			DownloadsDir = DEFAULT_DOWNLOADS_DIR;
			fileNameCreator = new HashCodeFileNameCreator();
		}

		public Builder setDownLoadDir(File downloadsDir) {
			this.DownloadsDir = downloadsDir;
			return this;
		}

		public Builder setFileNameScheme(FileNameCreator fileNameCreator) {
			this.fileNameCreator = fileNameCreator;
			return this;
		}

		public DownloaderOptions build() {
			return new DownloaderOptions(this);
		}

	}

}
