package org.hidaron.asyncdownloader.download;

/**
 * 
 * @author hidaron <hidaron@163.com>
 * 
 */
public class DownloaderInfo {

	private DownloadStatus status;
	public DownloadEntityInfo entityInfo;
	private long downloadFirstTime;

	public static class DownloadEntityInfo {
		private String entityPath;
		private long entityLength;
		private long WrittenBytesLength;

		public String getEntityPath() {
			return entityPath;
		}

		public void setEntityPath(String entityPath) {
			this.entityPath = entityPath;
		}

		public long getEntityLength() {
			return entityLength;
		}

		public void setEntityLength(long entityLength) {
			this.entityLength = entityLength;
		}

		public long getWrittenBytesLength() {
			return WrittenBytesLength;
		}

		public void setWrittenBytesLength(long writtenBytesLength) {
			WrittenBytesLength = writtenBytesLength;
		}
	}

	public DownloaderInfo() {
		status = DownloadStatus.NOTSTART;
		entityInfo = new DownloadEntityInfo();
	}

	public DownloadStatus getStatus() {
		return status;
	}

	public void setStatus(DownloadStatus status) {
		this.status = status;
	}

	public DownloadEntityInfo getEntityInfo() {
		return entityInfo;
	}

	public long getDownloadFirstTime() {
		return downloadFirstTime;
	}

	public void setDownloadFirstTime(long downloadFirstTime) {
		this.downloadFirstTime = downloadFirstTime;
	}

}
