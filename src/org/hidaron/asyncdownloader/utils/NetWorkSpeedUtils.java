package org.hidaron.asyncdownloader.utils;

/**
 * 
 * @author hidaron <hidaron@163.com>
 *
 */
public class NetWorkSpeedUtils {

	public static String speedFormatter(long speed) {
		if (speed > 1024) {
			return String.format("%2.1f%s", speed / 1024.0, "MB/s");
		} else {
			return String.format("%d%s", speed, "KB/s");
		}
	}
}
