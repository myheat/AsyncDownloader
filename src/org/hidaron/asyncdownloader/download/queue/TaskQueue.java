package org.hidaron.asyncdownloader.download.queue;

import org.hidaron.asyncdownloader.download.Downloader;

/**
 * 
 * @author hidaron <hidaron@163.com>
 * 
 */
public interface TaskQueue {
	public static final int TASK_ACTION_ADD = 1;
	public static final int TASK_ACTION_REMOVE = 2;
	public static final int TASK_ACTION_UPDATE = 3;
	public static final int TASK_ACTION_REMOVE_ALL = 4;

	public boolean add(Downloader downloader);

	public boolean update(Downloader downloader);

	public boolean remove(Downloader downloader);

	public void removeAll();

	public boolean contains(Downloader downloader);

	public int getLimit();

	public void notifyTask(int taskAction);

}
