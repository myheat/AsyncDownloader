package org.hidaron.asyncdownloader.download;

import java.io.File;

import org.hidaron.asyncdownloader.download.listener.OnDownloadListener;

/**
 * 
 * @author hidaron <hidaron@163.com>
 * 
 */
public interface Downloader {

	public void download();

	public void pause();

	public void redownload();

	public void cancel();
	
	public File getTemporaryFile();

	public boolean deleteTemporaryFile();

	public String getFileUrl();

	public DownloaderInfo getInfo();

	public void setDownloadStatus(DownloadStatus status);

	void setOnDownloadListener(OnDownloadListener onDownloadListener);

	public OnDownloadListener getOnDownloadListener();

}
